<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	protected function redirectTo($url, $statusCode = 302)
    {
        return Redirect::to($url, $statusCode);
    }

    protected function redirectRoute($route, $data = array())
    {
        return Redirect::route($route, $data);
    }

    protected function redirectBack($data = array())
    {
        return Redirect::back()->withInput()->with($data);
    }

    protected function redirectBackWithErrors(\Base $model)
    {
        return Redirect::back()->withInput()->withErrors($model->getValidator());
    }

}
