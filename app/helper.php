<?php


class Helper {

    /**
     * Comprueba si existe un módulo
     * @param  [type] $module [description]
     * @return [type]         [description]
     */
    public static function moduleExists($module = null)
    {
        $module = (isset($module)) ? $module : null;
        $root   = __DIR__ . '/modules/';

        return file_exists($root . $module);
    }



}