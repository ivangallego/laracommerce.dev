<?php


class Base extends Eloquent
{

    protected $validator;
    protected $guarded = array();

    protected $rules = array();
    protected $updateRules = array();

    protected $mapHeader = array();
    protected $dataTableFields = array();
    protected $presenters = array();

















    /**
     *
     * @param  string  $where [description]
     * @return boolean        [description]
     */
    public function isValid($where = 'create')
    {
        if ( ! isset($this->rules)) throw new Exception("No validation rule array defined in class " . $get_called_class() );

        $this->validator = Validator::make($this->getAttributes(), $this->getPreparedRules());

        return $this->validator->passes();
    }


    /**
     * [getPreparedRules description]
     * @return [type] [description]
     */
    protected function getPreparedRules()
    {
        return $this->replaceIdsIfExists($this->rules);
    }

    /**
     * [getPreparedRules description]
     * @return [type] [description]
     */
    public function getValidator()
    {
        if ( ! $this->validator) {
            throw new Exception('No hay nada instanciado de errores');
        }

        return $this->validator;
    }

    /**
     * [replaceIdsIfExists description]
     * @param  [type] $rules [description]
     * @return [type]        [description]
     */
    protected function replaceIdsIfExists($rules)
    {
        $newRules = [];

        foreach ($rules as $key => $rule):

            if (str_contains($rule, 'unique') and !is_null($this->id)) $rule .= ',' . $this->id;

            if (str_contains($rule, '<id>')):
                $replacement = $this->exists ? $this->getAttribute($this->primaryKey) : '';
                $rule = str_replace('<id>', $replacement, $rule);
            endif;

            array_set($newRules, $key, $rule);

        endforeach;

        return $newRules;
    }



    /**
     * Save info of given Model
     * @param  array  $options [description]
     * @return [type]          [description]
     */
    public function save(array $options = array())
    {
        return parent::save($options);
    }

}