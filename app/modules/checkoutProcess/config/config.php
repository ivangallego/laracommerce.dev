<?php

return array(

    # Redirección al iniciar sesión
    'urlStepOne'            => 'checkoutProcess::stepOne',    # URL::to('/')
    'urlUpdateItemCart'     => 'checkoutProcess::urlUpdateItemCart',
    'urlDeleteItemCart'     => 'checkoutProcess::urlDeleteItemCart',
    'urlStepTwo'            => 'checkoutProcess::stepTwo',
    'urlLoginPost'          => 'checkoutProcess::loginPost',
    'urlFormPost'           => 'checkoutProcess::stepTwo',
    'urlStepThree'          => 'checkoutProcess::stepThree',

    'viewForStep1'          => 'checkoutProcess.step1',
    'viewForStep2'          => 'checkoutProcess.step2',
    'viewForStep3'          => 'checkoutProcess.step3'
);