<?php namespace App\Modules\checkoutProcess\Controllers;

use Validator, Input, Config, URL, Redirect, Session, View, Auth, Helper, Cart, Hash;
use App\Modules\frontendUsers\Models\User as User;

class CartsController extends \BaseController
{


    /**
     * [getStepOne description]
     * @return [type] [description]
     */
    public function getStepOne()
    {
        $theCart        = Cart::instance('shopping')->content();
        $countCart      = Cart::instance('shopping')->count();
        $totalCart      = Cart::instance('shopping')->total();

        $urlUpdateCart  = URL::route(Config::get('checkoutProcess::config.urlUpdateItemCart'));
        $urlDeleteCart  = URL::route(Config::get('checkoutProcess::config.urlDeleteItemCart'));
        $urlStepTwo     = URL::route(Config::get('checkoutProcess::config.urlStepTwo'));

        return View::make(Config::get('checkoutProcess::viewForStep1'))
            ->with('theCart', $theCart)
            ->with('countCart', $countCart)
            ->with('totalCart', $totalCart)
            ->with('urlUpdateCart', $urlUpdateCart)
            ->with('urlDeleteCart', $urlDeleteCart)
            ->with('urlStepTwo', $urlStepTwo);
    }


    /**
     * [getStepTwo description]
     * @return [type] [description]
     */
    public function getStepTwo()
    {
        # Tienen que haber items en el carrito para que se pueda llegar a este paso.
        if (Cart::instance('shopping')->total() == 0) return Redirect::route(Config::get('checkoutProcess::config.urlStepOne'));

        $post           = (Auth::user()) ? Auth::user() : null;
        $urlLoginPost   = (Helper::moduleExists('frontendUsers')) ? Config::get('frontendUsers::config.urlLoginPost') : Config::get('checkoutProcess::config.urlLoginPost');

        $urlStepOne     = URL::route(Config::get('checkoutProcess::config.urlStepOne'));

        return View::make(Config::get('checkoutProcess::viewForStep2'))
            ->with('post', $post)
            ->with('urlLoginPost', $urlLoginPost)
            ->with('urlStepOne', $urlStepOne);
    }


    /**
     * [postStepTwo description]
     * @return [type] [description]
     */
    public function postStepTwo()
    {
        # Si no tiene carrito, no puede estar en el paso 2!!
        $urlStepOne     = Config::get('checkoutProcess::config.urlStepOne');
        $hasItemsInCart = Cart::instance('shopping')->count();
        if ($hasItemsInCart == 0) return Redirect::route($urlStepOne);

        $mailExists     = User::mailExists(Input::get('email'))->first();

        $rules = array(
            'email'         => 'email|max:175|required',
            'password'      => 'max:125',
            'first_name'    => 'required|max:175',
            'last_name'     => 'required|max:175',
            'telephone'     => 'required|max:55',
            'address'       => 'required|max:175',
            'city'          => 'required|max:175',
            'postalcode'    => 'required|max:175',
            'province'      => 'required|max:175',

            # Sending
            'sending_email'         => 'email|max:175|required',
            'sending_password'      => 'max:125',
            'sending_first_name'    => 'required|max:175',
            'sending_last_name'     => 'required|max:175',
            'sending_telephone'     => 'required|max:55',
            'sending_address'       => 'required|max:175',
            'sending_city'          => 'required|max:175',
            'sending_postalcode'    => 'required|max:175',
            'sending_province'      => 'required|max:175',
        );

        if (!$mailExists) $rules['password'] = 'required|max:125';

        # Devuelve al paso 2 con errores en caso que los haya.
        $validateFields    = Validator::make(Input::all(), $rules);
        if ($validateFields->fails()) return Redirect::back()->withInput(Input::all())->withErrors($validateFields->errors());


        foreach (Input::except('_token') as $keyInput => $valueInput):
            if (strpos($keyInput, 'sending_') === false and strpos($keyInput, 'same_as_') === false) $userFields[$keyInput] = $valueInput;
        endforeach;


        # Primero creamos la cuenta si no existe.
        if (!User::mailExists(Input::get('email'))->first()):

            if (Input::get('password') == '') return Redirect::back()->withInput(Input::all())->withErrors($validateFields->errors());

            $userFields['password'] = Hash::make($userFields['password']);
            $user = new User;
            $user->fill($userFields);

            if ($user->isValid()):
                $user->save();
            endif;

            Auth::loginUsingId($user->id);
        endif;


        if (Auth::guest()):

            if (!Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))):
                return Redirect::back()->withInput(Input::all())->withErrors(array('koLogin' => trans('frontendUsers::messages.wrong_account')));
            endif;

        endif;


        if (Auth::check()):

            $user = Auth::user();

            $user->first_name   = $userFields['first_name'];
            $user->last_name    = $userFields['last_name'];
            $user->telephone    = $userFields['telephone'];
            $user->address      = $userFields['address'];
            $user->city         = $userFields['city'];
            $user->postalcode   = $userFields['postalcode'];
            $user->province     = $userFields['province'];

            $user->save();

        endif;


        $urlStepThree = Config::get('checkoutProcess::config.urlStepThree');
        return Redirect::route($urlStepThree);
    }


    /**
     * [getStepThree description]
     * @return [type] [description]
     */
    public function getStepThree()
    {
         # Si no tiene carrito, no puede estar en el paso 3!!
        $urlStepOne     = Config::get('checkoutProcess::config.urlStepOne');
        $hasItemsInCart = Cart::instance('shopping')->count();
        if ($hasItemsInCart == 0 || Auth::guest())  return Redirect::route($urlStepOne);

        $theCart        = Cart::instance('shopping')->content();
        $countCart      = Cart::instance('shopping')->count();
        $totalCart      = Cart::instance('shopping')->total();

        $urlStepTwo     = URL::route(Config::get('checkoutProcess::config.urlStepTwo'));

        return View::make(Config::get('checkoutProcess::viewForStep3'))
            ->with('theCart', $theCart)
            ->with('countCart', $countCart)
            ->with('totalCart', $totalCart)
            ->with('urlStepOne', $urlStepOne)
            ->with('urlStepTwo', $urlStepTwo);
    }



    /**
     * [postUpdateCart description]
     * @return [type] [description]
     */
    public function postUpdateItemInCart()
    {
        $rules      = array('id' => 'required', 'qty' => 'required|min:1');
        $validation = Validator::make(Input::all(), $rules);
        $itemExists = Cart::instance('shopping')->search(array('id' => Input::get('id')));

        if ($validation->passes() and $itemExists)
            Cart::instance('shopping')->update($itemExists[0], Input::get('qty'));

        return $this->redirectBack();
    }


    /**
     * [postDeleteItemInCart description]
     * @return [type] [description]
     */
    public function postDeleteItemInCart()
    {
        $rules      = array('id' => 'required');
        $validation = Validator::make(Input::all(), $rules);
        $itemExists = Cart::instance('shopping')->search(array('id' => Input::get('id')));

        if ($validation->passes() and $itemExists)
            Cart::instance('shopping')->remove($itemExists[0]);

        return $this->redirectBack();
    }


    /**
     * [insertCart description]
     * @return [type] [description]
     */
    public function insertCart()
    {
        $rules      = array('id' => 'required', 'qty' => 'required|min:1', 'price' => 'required|numeric');
        $validation = Validator::make(Input::all(), $rules);

        if ($validation->passes()):

            $product            = false;   # Forzamos pero se podría buscar el producto por BD.
            $theProductName     = ($product) ? $product->name : Input::get('name');
            $theProductId       = Input::get('id');
            $qty                = Input::get('qty');
            $price              = Input::get('price');
            $urlOption          = base64_decode(Input::get('meta'));

            $itemExists = Cart::instance('shopping')->search(array('id' => $theProductId));

            # Si el producto existe actualizamos cantidad, sino, creamos fila.
            if ($itemExists):
                $oldQty = Cart::get($itemExists[0])->qty;
                Cart::instance('shopping')->update($itemExists[0], $oldQty + $qty);
            else:
                Cart::instance('shopping')->add($theProductId, $theProductName, $qty, $price, array('url' => $urlOption));
            endif;

            return Redirect::route(Config::get('checkoutProcess::config.urlStepOne'));

        endif;

        return $this->redirectBack(Input::all());
    }

}