<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Textos del módulo "checkoutProcess"
    |--------------------------------------------------------------------------
    |
    | Para mandar a traducir, copiar/pegar todos los textos de traducción
    | en un único archivo .php y parsearlo mediante PoEdit. Una vez recibido
    | el archivo .po desarrollar código de conversión para que Laravel lo pueda
    | interpretar y meterlo en la carpeta de su lang correspondiente.
    |
    */


    'step_intro'    => 'Paso de compra',

    'step_1'    => array('toString' => 'One',   'toInt' => '1'),
    'step_2'    => array('toString' => 'Two',   'toInt' => '2'),
    'step_3'    => array('toString' => 'Three', 'toInt' => '3'),

    'already_account' => '¿Ya tienes cuenta? Inicia sesión',
    'facturation_title' => 'Datos de facturación',
    'sending_title' => 'Datos de envío',

    'update' => 'Modificar',
    'your_cart' => 'Tu Carrito',
    'your_info' => 'Tus Datos',
    'payment_method' => 'Método de pago',


    'fields' => array(

        'email'         => array('title' => 'Email',        'placeholder' => 'Ej. ivan.gallego@thatzad.com'),
        'password'      => array('title' => 'Contraseña',   'placeholder' => 'Ej. ********'),
        'first_name'    => array('title' => 'Nombre',       'placeholder' => 'Ej. Iván'),
        'last_name'     => array('title' => 'Apellidos',    'placeholder' => 'Ej. Gallego Sánchez'),
        'telephone'     => array('title' => 'Teléfono',     'placeholder' => 'Ej. 682 31 35 14'),
        'address'       => array('title' => 'Dirección',    'placeholder' => 'Ej. C/ Saragossa, local 16'),
        'city'          => array('title' => 'Ciudad',       'placeholder' => 'Ej. Castelldefels'),
        'postalcode'    => array('title' => 'Cód. Postal',  'placeholder' => 'Ej. 08860'),
        'province'      => array('title' => 'Provincia',    'placeholder' => 'Ej. Barcelona'),
        'country'       => array('title' => 'País',         'placeholder' => 'Ej. Spain'),

        'same_as_facturation' => 'Son los mismos datos que los de facturación',


    ),
);