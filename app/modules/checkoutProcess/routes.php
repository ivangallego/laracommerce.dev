<?php

$root = 'App\Modules\checkoutProcess\Controllers\CartsController';


/* AÑADIR AL CARRITO */
Route::post('checkout-add-to-cart', array('as' => 'checkoutProcess::add_to_cart',   'uses' => $root . '@insertCart'));


/* PASOS DE COMPRA */
Route::get('paso-1-de-compra',          array('as' => 'checkoutProcess::stepOne',              'uses' => $root . '@getStepOne'));

Route::get('paso-2-de-compra',          array('as' => 'checkoutProcess::stepTwo',              'uses' => $root . '@getStepTwo'));
Route::post('paso-2-de-compra',         array('as' => 'checkoutProcess::stepTwo',              'uses' => $root . '@postStepTwo'));

Route::get('paso-3-de-compra',          array('as' => 'checkoutProcess::stepThree',             'uses' => $root . '@getStepThree'));


Route::post('actualizar-item-carrito',  array('as' => 'checkoutProcess::urlUpdateItemCart',    'uses' => $root . '@postUpdateItemInCart'));
Route::post('eliminar-item-carrito',    array('as' => 'checkoutProcess::urlDeleteItemCart',    'uses' => $root . '@postDeleteItemInCart'));
