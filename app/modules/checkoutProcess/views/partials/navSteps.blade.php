<?php $currentStep = (isset($currentStep)) ? $currentStep : 1; ?>

<nav class="steps" style="display:block; margin:2em auto; height:1em; width:29em; ">
    <ul>
        @for ($i=1; $i<=3; $i++)
        <?php
            $iToString      = trans('checkoutProcess::messages.step_' . $i . '.toString');
            $stepActive     = ($currentStep == $i)  ? 'active' : null;
            $urlNextStep    = ($currentStep > $i)   ? URL::route('checkoutProcess::step' . $iToString) : '#';
        ?>
        <li class="step_{{$i}} {{$stepActive}}" style="list-style:none; float:left; margin-right:1em;">
            <a href="{{ $urlNextStep }}">{{ trans('checkoutProcess::messages.step_intro') }} {{ $i }}</a>
        </li>

        @endfor
    </ul>
</nav>
