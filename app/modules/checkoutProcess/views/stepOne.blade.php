
@include('checkoutProcess::partials.navSteps', array('currentStep' => 1))

<table cellpadding="0" cellspacing="0" width="90%" align="center" style="margin-top:50px;">
    <thead>
        <tr>
            <th>Product</th>
            <th>Qty</th>
            <th>Item Price</th>
            <th>Subtotal</th>
            <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>

    @if ($countCart > 0)
    @foreach ($theCart as $row)

        <tr>
            <td>
                @if ($row->options->has('url'))
                    <p><a href="{{$row->options->url}}"><strong>{{$row->name}}</strong></a></p>
                @else
                    <p><strong>{{$row->name}}</strong></p>
                @endif
            </td>
            <td>
                {{ Form::open(array('url' => $urlUpdateCart))}}

                <input type="text" name="qty" value="{{ $row->qty }}" size="3" style="text-align:center;">
                <input type="hidden" name="id" value="{{ $row->id }}" />
                <input type="submit" value="Cambiar" id="updateCartSubmit" style="background:none; border:none;" />

                {{ Form::close() }}

            </td>
            <td>{{ $row->price }}€</td>
            <td>{{ $row->subtotal }}€</td>
            <td>
                {{ Form::open(array('url' => $urlDeleteCart))}}
                <input type="hidden" name="id" value="{{ $row->id }}" />
                <input type="submit" value="Eliminar producto del carrito" id="deleteCartSubmit" style="background:none; border:none; color:blue;" />

                {{ Form::close() }}
            </td>
       </tr>

    @endforeach
    @else
        <tr valig="middle">
            <td align="center" colspan="5" height="60">No hay productos en el carrito</td>
        </tr>
    @endif


        <!-- TOTALIZE -->
        <tr valig="middle">
            <td align="right" colspan="4" height="60">TOTAL: {{ $totalCart}}€</td>
        </tr>

    </tbody>
</table>

<div style="display:block; margin:0 auto; width:90%;">

    <a href="/" style="float:left; text-transform:uppercase;">Seguir comprando</a>

    @if ($countCart > 0)
    <a href="{{ $urlStepTwo }}"  style="float:right; text-transform:uppercase;">Siguiente Paso</a>
    @endif
</div>
