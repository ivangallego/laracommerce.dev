@extends('templates.template')


@section('content')
<h1>Carrito de compra</h1>
<p>Pequeña prueba del paso 3 de compra de un carrito</p>

@include('checkoutProcess::partials.navSteps', array('currentStep' => 3))


<ul id="cart-resume">

    <li>
        <strong>
            {{ trans('checkoutProcess::messages.your_cart') }}
            &nbsp;
            <small><a href="{{ URL::route($urlStepOne) }}">{{ trans('checkoutProcess::messages.update') }}</a></small>
        </strong>

        @foreach ($theCart as $row)

        <div style="display:block; text-align:left; margin:1em 0 2em 0;">
            @if ($row->options->has('url'))
                <span class="item-name"><a href="{{ $row->options->url }}">{{ $row->name }}</a></span>
            @else
                <span class="item-name">{{ $row->name }}</span>
            @endif

            <br/>
            <small>{{ $row->qty }} prod. a {{ $row->price }} €/ud.</small><br/>
            <small>Subtotal: {{ number_format($row->subtotal, 2, '.', '') }} €</small>
        </div>

        @endforeach

    </li>


    <li>
        <strong>
            {{ trans('checkoutProcess::messages.your_info') }}
            &nbsp;
            <small><a href="{{ URL::route($urlStepOne) }}">{{ trans('checkoutProcess::messages.update') }}</a></small>
        </strong>

        <p>
            <small></small>
        </p>
    </li>


    <li>
        <strong>
            {{ trans('checkoutProcess::messages.payment_method') }}
            &nbsp;
            <small><a href="{{ URL::route($urlStepOne) }}">{{ trans('checkoutProcess::messages.update') }}</a></small>
        </strong>

    </li>

</ul>

@stop