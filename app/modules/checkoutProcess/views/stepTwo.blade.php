@extends('templates.template')


@section('content')
<h1>Carrito de compra</h1>
<p>Pequeña prueba del paso 1 de compra de un carrito</p>

@include('checkoutProcess::partials.navSteps', array('currentStep' => 2, 'redirect' => $redirect))


    @if (Auth::guest())

        <p>{{ trans('checkoutProcess::messages.already_account') }}</p>

        @if(Session::get('ko'))
            <fieldset class="no_label error">
              <div>{{ trans('checkoutProcess::messages.wrong_account') }}</div>
            </fieldset>
        @endif

        @include('frontendUsers::formPartials.login')
    @endif


    {{ Form::model($post) }}

    <div style="float:left; width:48%; margin-top:2em;" class="left">
        <h3>{{ trans('checkoutProcess::messages.facturation_title') }}</h3>

        <label for="email" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.email.title') }}</span>
            {{ Form::text('email', null, array('id' => 'email', 'placeholder' => trans('checkoutProcess::messages.fields.email.title')))}}
            {{ $errors->first('email', '<span class="error">:message</span>') }}
        </label>

        @if (Auth::guest())
            <label for="password" style="display:block; margin:1em 0;">
                <span>{{ trans('checkoutProcess::messages.fields.password.title') }}</span>
                {{ Form::password('password', null, array('id' => 'password', 'placeholder' => trans('checkoutProcess::messages.fields.password.title')))}}
                {{ $errors->first('password', '<span class="error">:message</span>') }}
                @if(Session::get('koLogin'))
                    {{ Session::get('koLogin')}}
                @endif
            </label>
        @endif

        <label for="first_name" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.first_name.title') }}</span>
            {{ Form::text('first_name', null, array('id' => 'first_name', 'placeholder' => trans('checkoutProcess::messages.fields.first_name.title')))}}
            {{ $errors->first('first_name', '<span class="error">:message</span>') }}
        </label>

        <label for="last_name" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.last_name.title') }}</span>
            {{ Form::text('last_name', null, array('id' => 'last_name', 'placeholder' => trans('checkoutProcess::messages.fields.last_name.title')))}}
            {{ $errors->first('last_name', '<span class="error">:message</span>') }}
        </label>

        <label for="telephone" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.telephone.title') }}</span>
            {{ Form::text('telephone', null, array('id' => 'telephone', 'placeholder' => trans('checkoutProcess::messages.fields.telephone.title')))}}
            {{ $errors->first('telephone', '<span class="error">:message</span>') }}
        </label>

        <label for="address" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.address.title') }}</span>
            {{ Form::text('address', null, array('id' => 'address', 'placeholder' => trans('checkoutProcess::messages.fields.address.title')))}}
            {{ $errors->first('address', '<span class="error">:message</span>') }}
        </label>

        <label for="city" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.city.title') }}</span>
            {{ Form::text('city', null, array('id' => 'city', 'placeholder' => trans('checkoutProcess::messages.fields.city.title')))}}
            {{ $errors->first('city', '<span class="error">:message</span>') }}
        </label>

        <label for="postalcode" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.postalcode.title') }}</span>
            {{ Form::text('postalcode', null, array('id' => 'postalcode', 'placeholder' => trans('checkoutProcess::messages.fields.postalcode.title')))}}
            {{ $errors->first('postalcode', '<span class="error">:message</span>') }}
        </label>

        <label for="province" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.province.title') }}</span>
            {{ Form::text('province', null, array('id' => 'province', 'placeholder' => trans('checkoutProcess::messages.fields.province.title')))}}
            {{ $errors->first('province', '<span class="error">:message</span>') }}
        </label>


        @if (Helper::moduleExists('frontendUsers'))
        @if (Config::get('frontendUsers::config.withCountries'))
            <label for="country" style="display:block; margin:1em 0;">
                <span>{{ trans('checkoutProcess::messages.fields.country.title') }}</span>
                {{ Form::text('country', null, array('id' => 'country', 'placeholder' => trans('checkoutProcess::messages.fields.country.title')))}}
                {{ $errors->first('country', '<span class="error">:message</span>') }}
            </label>
        @endif
        @endif

    </div>


    <div style="float:right; width:48%; margin-top:2em;" class="right">
        <h3>{{ trans('checkoutProcess::messages.sending_title') }}</h3>

        <label for="same_as_facturation">
            <input type="checkbox" name="same_as_facturation" id="same_as_facturation" value="1" >
            <span>{{ trans('checkoutProcess::messages.fields.same_as_facturation') }}</span>
        </label>

        <label for="sending_email" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.email.title') }}</span>
            {{ Form::text('sending_email', null, array('id' => 'sending_email', 'placeholder' => trans('checkoutProcess::messages.fields.email.title')))}}
            {{ $errors->first('sending_email', '<span class="error">:message</span>') }}
        </label>


        <label for="sending_first_name" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.first_name.title') }}</span>
            {{ Form::text('sending_first_name', null, array('id' => 'sending_first_name', 'placeholder' => trans('checkoutProcess::messages.fields.first_name.title')))}}
            {{ $errors->first('sending_first_name', '<span class="error">:message</span>') }}
        </label>

        <label for="sending_last_name" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.last_name.title') }}</span>
            {{ Form::text('sending_last_name', null, array('id' => 'sending_last_name', 'placeholder' => trans('checkoutProcess::messages.fields.last_name.title')))}}
            {{ $errors->first('sending_last_name', '<span class="error">:message</span>') }}
        </label>

        <label for="sending_telephone" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.telephone.title') }}</span>
            {{ Form::text('sending_telephone', null, array('id' => 'sending_telephone', 'placeholder' => trans('checkoutProcess::messages.fields.telephone.title')))}}
            {{ $errors->first('sending_telephone', '<span class="error">:message</span>') }}
        </label>

        <label for="sending_address" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.address.title') }}</span>
            {{ Form::text('sending_address', null, array('id' => 'sending_address', 'placeholder' => trans('checkoutProcess::messages.fields.address.title')))}}
            {{ $errors->first('sending_address', '<span class="error">:message</span>') }}
        </label>

        <label for="sending_city" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.city.title') }}</span>
            {{ Form::text('sending_city', null, array('id' => 'sending_city', 'placeholder' => trans('checkoutProcess::messages.fields.city.title')))}}
            {{ $errors->first('sending_city', '<span class="error">:message</span>') }}
        </label>

        <label for="sending_postalcode" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.postalcode.title') }}</span>
            {{ Form::text('sending_postalcode', null, array('id' => 'sending_postalcode', 'placeholder' => trans('checkoutProcess::messages.fields.postalcode.title')))}}
            {{ $errors->first('sending_postalcode', '<span class="error">:message</span>') }}
        </label>

        <label for="sending_province" style="display:block; margin:1em 0;">
            <span>{{ trans('checkoutProcess::messages.fields.province.title') }}</span>
            {{ Form::text('sending_province', null, array('id' => 'sending_province', 'placeholder' => trans('checkoutProcess::messages.fields.province.title')))}}
            {{ $errors->first('sending_province', '<span class="error">:message</span>') }}
        </label>


        @if (Helper::moduleExists('frontendUsers'))
        @if (Config::get('frontendUsers::config.withCountries'))
            <label for="country" style="display:block; margin:1em 0;">
                <span>{{ trans('checkoutProcess::messages.fields.country.title') }}</span>
                {{ Form::text('country', null, array('id' => 'country', 'placeholder' => trans('checkoutProcess::messages.fields.country.title')))}}
                {{ $errors->first('country', '<span class="error">:message</span>') }}
            </label>
        @endif
        @endif

    </div>


    <div style="display:block; padding-top:3em;  margin:0 auto; width:80%; clear:both;">

        <a href="{{ $urlStepOne }}" style="float:left; text-transform:uppercase;">Volver al carrito</a>

        <input type="submit" value="Siguiente Paso" style="float:right;">
    </div>



    {{ Form::close() }}



@stop