<?php

return array(

    # Redirección al iniciar sesión
    'loginRedirect' => URL::route('frontendUsers::account'),    # URL::to('/')

    'urlLoginPost'  => 'frontendUsers::login',

    'withCountries' => false,
);