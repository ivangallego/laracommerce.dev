<?php namespace App\Modules\frontendUsers\Controllers;

use View, Password, Input, Lang, App, Hash, Redirect;


class RemindersController extends \Controller {

    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function getRemind()
    {
        return View::make('frontendUsers.remind');
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind()
    {
        switch ($response = Password::remind(Input::only('email')))
        {
            case Password::INVALID_USER:
                return Redirect::back()->with('error', trans('frontendUsers::messages.reset_pass.invalid_user'));

            case Password::REMINDER_SENT:
                return Redirect::back()->with('ok', trans('frontendUsers::messages.reset_pass.reminder_sent'));
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return Response
     */
    public function getReset($token = null)
    {
        if (is_null($token)) App::abort(404);

        return View::make('frontendUsers.reset')->with('token', $token);
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset()
    {
        $credentials = Input::only('email', 'password', 'password_confirmation', 'token');

        $response = Password::reset($credentials, function($user, $password)
        {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response)
        {
            case Password::INVALID_PASSWORD:
                return Redirect::back()->with('error', trans('frontendUsers::messages.reset_pass.invalid_password'));

            case Password::INVALID_TOKEN:
                return Redirect::back()->with('error', trans('frontendUsers::messages.reset_pass.invalid_token'));

            case Password::INVALID_USER:
                return Redirect::back()->with('error', trans('frontendUsers::messages.reset_pass.invalid_user'));

            case Password::PASSWORD_RESET:
                return Redirect::back()->with('ok', trans('frontendUsers::messages.reset_pass.password_reset'));
        }
    }

}
