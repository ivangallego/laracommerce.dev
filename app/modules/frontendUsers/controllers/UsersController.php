<?php namespace App\Modules\frontendUsers\Controllers;

use View, Validator, Input, Redirect, Auth, Session, Config, DB, Hash;


class UsersController extends \BaseController
{
    protected $user;


    public function getRegister()
    {
        return View::make('frontendUsers.account')
            ->with('user', $this->user)
            ->with('type', 'register');
    }


    public function postRegister()
    {
        $fields = Input::all();
        $user   = $this->user;

        $fields['password'] = (!is_null($fields['password'])) ? Hash::make($fields['password']) : null;
        $user->fill($fields);

        if ($user->isValid()) {
            $user->save();

            return $this->redirectBack(array('ok' => 'Tu cuenta se ha creado con éxito.<br/>Te hemos enviado un mail.'));
        }

        return $this->redirectBackWithErrors($user);
    }


    public function getAccount()
    {
        $user = $this->user->find(Auth::user()->id);

        return View::make('frontendUsers.account')
            ->with('user', $user)
            ->with('type', 'account');
    }


    public function postAccount()
    {
        $fields = Input::all();
        $user   = $this->user->find(Auth::user()->id);

        if ($fields['password'] != ''):
            $fields['password'] = Hash::make($fields['password']);
        else:
            $oldPass = DB::table('users')->where('id', Auth::user()->id)->pluck('password');
            $fields['password'] = $oldPass;
        endif;

        $user->fill($fields);

        if ($user->isValid()):
            $user->save();

            return $this->redirectBack(array('ok' => 'Tu cuenta se ha modificado con éxito'));
        endif;


        return $this->redirectBackWithErrors($user);
    }


    /**
     * Get the Login's form
     * @return [type] [description]
     */
    public function getLogin()
    {
        $post['login_email']    = isset($post['login_email'])       ? $post['login_email'] : null;
        $post['login_password'] = isset($post['login_password'])    ? $post['login_password'] : null;

        $urlLoginPost = Config::get('frontendUsers::urlLoginPost');

        return View::make('frontendUsers.login')
            ->with('post', $post)
            ->with('urlLoginPost', $urlLoginPost);
    }



    public function postLogin()
    {
        $rules      = array('username' => 'required|email', 'password' => 'required');
        $validation = Validator::make(Input::all(), $rules);
        $redirect   = Input::get('redirect', Config::get('frontendUsers::loginRedirect'));


        # Validation fails
        if ($validation->fails()) return Redirect::back()->with('ko', true);

        # 1) Check if email exists on DB
        $user = $this->user->mailExists(Input::get('username'))->first();

        # 2) Check if user's status is 1
        if ($user and $user->status == 1):

            # 3) Check password
            if (Auth::attempt(array('email' => Input::get('username'), 'password' => Input::get('password')), true)):

                $user->updateLoginInfo(Input::all(), $user->id);
                return Redirect::to($redirect);

            endif;
        endif;

        return Redirect::back()->with('ko', true);
    }


    public function getLogout()
    {
        Auth::logout();
        return Redirect::to('/');
    }


    public function __construct(\App\Modules\frontendUsers\Models\User $user)
    {
        $this->user = $user;
    }


}