<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Textos del módulo "frontendUsers"
    |--------------------------------------------------------------------------
    |
    | Para mandar a traducir, copiar/pegar todos los textos de traducción
    | en un único archivo .php y parsearlo mediante PoEdit. Una vez recibido
    | el archivo .po desarrollar código de conversión para que Laravel lo pueda
    | interpretar y meterlo en la carpeta de su lang correspondiente.
    |
    */


    'welcome'       => 'Bienvenido, :first_name',
    'logout'        => 'Cerrar sesión',
    'login'         => 'Iniciar sesión',
    'wrong_account' => 'Usuario y/o contraseña no válidos',

    'my_account' => array(
        'title_account'         => 'Mi Cuenta',
        'title_register'        => 'Registro',

        'submit_account'        => 'Guardar Cambios',
        'submit_register'       => 'Registrarse',

        'paragraph_1_account'   => 'Aquí se podría poner un texto describiendo cómo actualizar los datos de mi cuenta, etc...<br/>Aquí se podría poner un texto describiendo cómo actualizar los datos de mi cuenta, etc...',
        'paragraph_1_register'  => 'Aquí se podría poner un texto describiendo cómo registrarse, etc...<br/>Aquí se podría poner un texto describiendo cómo registrarse, etc...',
    ),

    'reset_pass' => array(
        'reminder_sent'     => 'Te hemos enviado un correo electrónico explicándote los pasos a seguir para recuperar tu cuenta.',
        'invalid_password'  => 'Contraseña no válida.',
        'invalid_token'     => 'El enlace ha caducado. Por favor, vuelve a iniciar el proceso.',
        'invalid_user'      => 'El usuario no es válido.',
        'password_reset'    => 'La contraseña se ha modificado correctamente. Ya puedes volver a iniciar sesión con ella.',
    ),

    'fields' => array(

        'email'                 => array('title' => 'Email',                'placeholder' => 'Ej: ivan.gallego@thatzad.com'),
        'password'              => array('title' => 'Contraseña',           'placeholder' => 'Ej: 34sGA24haxW!%'),
        'password_confirmation' => array('title' => 'Repetir Contraseña',   'placeholder' => 'Ej: 34sGA24haxW!%'),
        'first_name'            => array('title' => 'Nombre',               'placeholder' => 'Ej: Iván'),
        'last_name'             => array('title' => 'Apellidos',            'placeholder' => 'Ej: Gallego Sánchez'),
        'telephone'             => array('title' => 'Teléfono',             'placeholder' => 'Ej: 93 654 13 61'),
        'address'               => array('title' => 'Dirección',            'placeholder' => 'Ej: C/Saragossa, local 16'),
        'city'                  => array('title' => 'Ciudad',               'placeholder' => 'Ej: Castelldefels'),
        'postalcode'            => array('title' => 'Código Postal',        'placeholder' => 'Ej: 08860'),
        'province'              => array('title' => 'Provincia',            'placeholder' => 'Ej: Barcelona'),

    ),

    'emails' => array(

        'reminder' => array(
            'subject'       => 'Solicitud de cambio de contraseña',
            'paragraph_1'   => 'Para resetear tu contraseña, por favor, clica en el siguiente enlace y rellena el formulario:',
        ),

    )

);