<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');

			# Login info
			$table->integer('group_id')->default(2);
			$table->string('email', 175)->unique();
			$table->string('password', 125);
			$table->integer('status')->default(1);
			$table->integer('logins')->default(0);
			$table->timestamp('last_login')->nullable();
			$table->string('last_ip', 75)->nullable();

			# User info
			$table->string('first_name', 175)->nullable();
			$table->string('last_name', 175)->nullable();
			$table->string('telephone', 55)->nullable();
			$table->string('address', 175)->nullable();
			$table->string('city', 175)->nullable();
			$table->string('postalcode', 175)->nullable();
			$table->string('province', 175)->nullable();
			$table->string('country', 55)->default('ES');
			$table->string('remember_token', 100)->nullable();

			$table->timestamps();
		});


		Schema::create('groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->unique();
			$table->timestamps();
		});


		# Create the groups (by default only admin & user)
		\DB::table('groups')->insert(array('name' => 'Admin'));
		\DB::table('groups')->insert(array('name' => 'User'));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
		Schema::dropIfExists('groups');
	}

}
