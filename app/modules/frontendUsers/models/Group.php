<?php namespace App\Modules\frontendUsers\Models;


class Group extends \Base {

    protected $table = 'groups';


    public function users()
    {
        return $this->hasMany('App\Modules\frontendUsers\Models\User');
    }

}