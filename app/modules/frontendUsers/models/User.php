<?php namespace App\Modules\frontendUsers\Models;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

use DB, Request;


class User extends \Base implements UserInterface, RemindableInterface
{

    protected $rules = array(

        # Login info
        'group_id'      => 'integer|min:1',
        'email'         => 'email|max:175|required|unique:users,email',
        'password'      => 'required|max:125',
        'status'        => 'integer',
        'logins'        => 'integer|min:1',
        'last_login'    => 'date',

        # User info
        'first_name'    => 'required|max:175',
        'last_name'     => 'max:175',
        'telephone'     => 'max:55',
        'address'       => 'max:175',
        'city'          => 'max:175',
        'postalcode'    => 'max:175',
        'province'      => 'max:175',
    );


    public function group()
    {
        return $this->hasOne('App\Modules\frontendUsers\Models\Group');
    }


    public function scopeIsAdmin($query)
    {
        return $query->where('group_id', '=', 1);
    }


    public function scopeMailExists($query, $mail)
    {
        return $query->where('email', '=', $mail);
    }







    /**
     * Actualiza la información de Login del usuario.
     * @param  array  $fields  [Array con la info de login]
     * @param  [type] $user_id [ID del usuario]
     * @return [type]          [Boolean]
     */
    public function updateLoginInfo($fields = array(), $user_id = null)
    {
        $user_id    = (isset($user_id)) ? $user_id : null;
        $user       = $this->find($user_id);

        if ($user):

            DB::table('users')->where('id', $user_id)->update(array(
                'logins'        => (int) $user->logins + 1,
                'last_login'    => date("Y-m-d H:i:s",time()),
                'last_ip'       => Request::getClientIp()
            ));

            return true;
        endif;

        return false;
    }




    /**
     * Save fields of given Model
     * @param  array  $options [description]
     * @return [type]          [description]
     */
    public function save(array $options = array())
    {
        $save = parent::save($options);

        return ($save) ? true : false;
    }




    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }


    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }


}