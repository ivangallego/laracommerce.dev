<?php

$root       = 'App\Modules\frontendUsers\Controllers\UsersController';
$reminder   = 'App\Modules\frontendUsers\Controllers\RemindersController';



# Login & Logout
Route::get('login',                             array('as' => 'frontendUsers::login',               'uses' => $root . '@getLogin'));
Route::post('login',                            array('as' => 'frontendUsers::login',               'uses' => $root . '@postLogin'));
Route::get('logout',                            array('as' => 'frontendUsers::logout',              'uses' => $root . '@getLogout',     'before' => 'auth'));

# Mi Cuenta
Route::get('mi-cuenta',                         array('as' => 'frontendUsers::account',             'uses' => $root . '@getAccount',    'before' => 'auth'));
Route::post('mi-cuenta',                        array('as' => 'frontendUsers::account',             'uses' => $root . '@postAccount',   'before' => 'auth'));

# Registro
Route::get('registro-de-usuario',               array('as' => 'frontendUsers::register',            'uses' => $root . '@getRegister'));
Route::post('registro-de-usuario',              array('as' => 'frontendUsers::register',            'uses' => $root . '@postRegister'));

# He olvidado mi contraseña
Route::get('he-olvidado-la-clave-de-acceso',    array('as' => 'frontendUsers::lost_pass_step_1',    'uses' => $reminder . '@getRemind'));
Route::post('he-olvidado-la-clave-de-acceso',   array('as' => 'frontendUsers::lost_pass_step_1',    'uses' => $reminder . '@postRemind'));
Route::get('nueva-clave-de-acceso/{token}',     array('as' => 'frontendUsers::lost_pass_step_2',    'uses' => $reminder . '@getReset'));
Route::post('nueva-clave-de-acceso/{token}',    array('as' => 'frontendUsers::lost_pass_step_2',    'uses' => $reminder . '@postReset'));