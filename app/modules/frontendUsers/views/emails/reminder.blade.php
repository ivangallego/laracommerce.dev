<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>{{ trans('frontendUsers.emails.reminder.subject') }}</h2>

        <div>
            {{ trans('frontendUsers.emails.reminder.paragraph_1')}}
            <br/><br/>
            <a href="{{ URL::route('frontendUsers::lost_pass_step_2', array($token)) }}">
                {{ URL::route('frontendUsers::lost_pass_step_2', array($token)) }}
            </a>
        </div>
    </body>
</html>
