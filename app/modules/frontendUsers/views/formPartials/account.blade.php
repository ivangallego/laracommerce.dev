{{ Form::model($user) }}

    <h3>{{ trans('frontendUsers::messages.my_account.title_' . $type) }}</h3>
    <p>{{ trans('frontendUsers::messages.my_account.paragraph_1_' . $type) }}</p>

    @if(Session::get('ok'))
      <fieldset class="no_label error">
        <div>{{ Session::get('ok') }}</div>
      </fieldset>
    @endif


    <label for="email">
        {{ $errors->first('email', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.email.title') }}</span>
        {{ Form::text('email', null, array('id' => 'email', 'placeholder' => trans('frontendUsers::messages.fields.email.placeholder'))) }}
    </label>

    <br/><br/>

    <label for="password">
        {{ $errors->first('password', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.password.title') }}</span>
        {{ Form::password('password', null, array('id' => 'password', 'placeholder' => trans('frontendUsers::messages.fields.password.placeholder'))) }}
    </label>

    <br/><br/>

    <label for="first_name">
        {{ $errors->first('first_name', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.first_name.title') }}</span>
        {{ Form::text('first_name', null, array('id' => 'first_name', 'placeholder' => trans('frontendUsers::messages.fields.first_name.placeholder'))) }}
    </label>

    <br/><br/>

    <label for="last_name">
        {{ $errors->first('last_name', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.last_name.title') }}</span>
        {{ Form::text('last_name', null, array('id' => 'last_name', 'placeholder' => trans('frontendUsers::messages.fields.last_name.placeholder'))) }}
    </label>

    <br/><br/>

    <label for="telephone">
        {{ $errors->first('telephone', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.telephone.title') }}</span>
        {{ Form::text('telephone', null, array('id' => 'telephone', 'placeholder' => trans('frontendUsers::messages.fields.telephone.placeholder'))) }}
    </label>

    <br/><br/>

    <label for="address">
        {{ $errors->first('address', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.address.title') }}</span>
        {{ Form::text('address', null, array('id' => 'address', 'placeholder' => trans('frontendUsers::messages.fields.address.placeholder'))) }}
    </label>

    <br/><br/>

    <label for="postalcode">
        {{ $errors->first('city', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.city.title') }}</span>
        {{ Form::text('city', null, array('id' => 'city', 'placeholder' => trans('frontendUsers::messages.fields.city.placeholder'))) }}
    </label>

    <br/><br/>

    <label for="postalcode">
        {{ $errors->first('postalcode', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.postalcode.title') }}</span>
        {{ Form::text('postalcode', null, array('id' => 'postalcode', 'placeholder' => trans('frontendUsers::messages.fields.postalcode.placeholder'))) }}
    </label>

    <br/><br/>

    <label for="province">
        {{ $errors->first('province', '<span class="error">:message</span>') }}
        <span>{{ trans('frontendUsers::messages.fields.province.title') }}</span>
        {{ Form::text('province', null, array('id' => 'province', 'placeholder' => trans('frontendUsers::messages.fields.province.placeholder'))) }}
    </label>

    <br/><br/>

    <input type="submit" value="{{ trans('frontendUsers::messages.my_account.submit_' . $type) }}" />

{{ Form::close() }}