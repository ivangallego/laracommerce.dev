  {{ Form::open(array('url' => URL::route($urlLoginPost), 'method' => 'post')) }}

    @if(Session::get('ko'))
      <fieldset class="no_label error">
        <div>{{ trans('frontendUsers::messages.wrong_account') }}</div>
      </fieldset>
    @endif

    <label for="login_email">
      <span>{{ trans('frontendUsers::messages.fields.email.title') }}</span>
      <input type="text" name="username" id="login_email" value="{{ $post['login_email'] }}" />
    </label>

    <label for="login_password">
      <span>{{ trans('frontendUsers::messages.fields.password.title') }}</span>
      <input type="password" name="password" id="login_password" value="{{ $post['login_password'] }}" />
    </label>

    <input type="submit" value="{{ trans('frontendUsers::messages.login') }}" />

    @if (isset($redirect))
      <input type="hidden" name="redirect" value="{{ $redirect }}" />
    @endif

    <br/><br/><br/>

  {{ Form::close() }}
