{{ Form::open() }}

    @if(Session::get('error'))
      <fieldset class="no_label error">
        <div>{{ Session::get('error') }}</div>
      </fieldset>
    @endif

    @if(Session::get('ok'))
      <fieldset class="no_label">
        <div>{{ Session::get('ok') }}</div>
      </fieldset>
    @endif


    <label for="email">
      <span>{{ trans('frontendUsers::messages.fields.email.title') }}</span>
      <input type="email" name="email" id="email" value="" />
    </label>

    <input type="submit" value="Enviar clave">

{{ Form::close() }}

