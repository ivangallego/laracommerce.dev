  {{ Form::open() }}

    @if(Session::get('error'))
      <fieldset class="no_label error">
        <div>{{ Session::get('error') }}</div>
      </fieldset>
    @endif

    @if(Session::get('ok'))
      <fieldset class="no_label">
        <div>{{ Session::get('ok') }}</div>
      </fieldset>
    @endif

    <label for="email">
      <span>{{ trans('frontendUsers::messages.fields.email.title') }}</span>
      <input type="email" name="email" id="email" value="" />
    </label>

    <label for="password">
      <span>{{ trans('frontendUsers::messages.fields.password.title') }}</span>
      <input type="password" name="password" id="password" value="" />
    </label>

    <label for="password_confirmation">
      <span>{{ trans('frontendUsers::messages.fields.password_confirmation.title') }}</span>
      <input type="password" name="password_confirmation" id="password_confirmation" value="" />
    </label>

    <input type="hidden" name="token" value="{{ $token }}">
    <input type="submit" value="Resetear Password">


  {{ Form::close() }}
