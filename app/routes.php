<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    return View::make('hello');
});


Route::get('producto', function(){

    $urlAddToCart = (Helper::moduleExists('checkoutProcess')) ? URL::route('checkoutProcess::add_to_cart') : URL::route('add_to_cart');

    return View::make('productoTest')
        ->with('urlAddToCart', $urlAddToCart);
});

Route::get('producto-dos', function(){
    $urlAddToCart = (Helper::moduleExists('checkoutProcess')) ? URL::route('checkoutProcess::add_to_cart') : URL::route('add_to_cart');

    return View::make('productTestTwo')
        ->with('urlAddToCart', $urlAddToCart);
});




Route::post('add-to-cart', array('as' => 'add_to_cart',  'uses' => 'ProductsController@insertCart'));
