@extends('templates.template')


@section('content')
    @include('checkoutProcess::stepTwo', array('redirect' => URL::route(Config::get('checkoutProcess::urlFormPost'))))
@stop


@section('scripts')
<script>
    $(function() {

        $("#same_as_facturation").click(function() {
            if($("#same_as_facturation").is(':checked')) {
                fillWithFacturationInfo();
            } else {
                deleteSendingInfo();
            }
        });

    });


    function fillWithFacturationInfo()
    {

        $('#content .left :input').each(function(count, field) {
            var field     = $(field);
            inputName   = 'sending_' + field.attr('name');

            $('input[name="' + inputName + '"]').val(field.val());
        });

        return false;
    }


    function deleteSendingInfo()
    {
        $('#content .right :input').each(function(count, field) {
            var field = $(field);
            field.val('');
        });

        return false;
    }

</script>
@stop