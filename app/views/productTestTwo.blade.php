@extends('templates.template')


@section('content')
<h1>Producto de prueba Two</h1>
<p>Este es otro producto de prueba para desarrollar el módulo de proceso de compra</p>

<strong>Precio: 3.99</strong>

{{ Form::open(array('url' => $urlAddToCart))}}

    <label for="qty" style="display:block; margin:3em 0 1em 0;">
        <span>Cantidad</span>
        <input type="text" name="qty" value="1" id="qty" size="2" style="text-align:center;" />
    </label>

    <input type="submit" value="Añadir al Carrito" />


    <input type="hidden" name="name" value="Producto de Prueba Two"/>
    <input type="hidden" name="id" value="94ffk44518"/>
    <input type="hidden" name="price" value="3.99" />
    <input type="hidden" name="meta" value="{{ base64_encode(URL::current()) }}" />

{{ Form::open() }}


@stop