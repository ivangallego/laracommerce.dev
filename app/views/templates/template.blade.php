<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Laracommerce.dev</title>
    <meta name="description" content="Laracommerce.dev">
    <meta name="viewport" content="width=1240">
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:700);

        body {
            margin:0;
            font-family:'Lato', sans-serif;
            text-align:center;
            color: #999;
        }

        header{ margin-top:50px;}

        .welcome {
            width: 300px;
            height: 200px;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -150px;
            margin-top: -100px;
        }

        a, a:visited {
            text-decoration:none;
        }

        h1 {
            font-size: 32px;
            margin: 16px 0 0 0;
        }

        #updateCartSubmit:hover{ text-decoration:underline; cursor:pointer; }
        #deleteCartSubmit:hover{ text-decoration:underline; cursor:pointer; }
        #cart-resume{ display:block; list-style:none; width:90%; height:100px; margin:0 auto; }
        #cart-resume li{ float:left; width:33%; }
        #cart-resume strong{ display:block; margin-bottom:1em; width:100%; }
        span.item-name a, span.item-name{ color:#000; }
    </style>
</head>
<body>

    @if (Helper::moduleExists('checkoutProcess'))
        <a href="{{URL::route('checkoutProcess::stepOne')}}">
            <span>{{ Cart::instance('shopping')->count() }}</span>
            productos en el carrito
        </a>
    @endif

    <header>
        @if (Auth::check())

            <div class="logged-user">
                <span>{{ trans('frontendUsers::messages.welcome', array('first_name' => Auth::user()->first_name)) }}</span>
                <span>|</span>
                <a href="{{ URL::route('frontendUsers::account') }}">{{ trans('frontendUsers::messages.my_account.title_account') }}</a>
                <span>|</span>
                <a href="{{ URL::route('frontendUsers::logout') }}">{{ trans('frontendUsers::messages.logout') }}</a>
            </div>

        @else
            <a href="{{URL::route('frontendUsers::login')}}">{{ trans('frontendUsers::messages.login')}}</a>
            &nbsp;&nbsp;
            <a href="{{URL::route('frontendUsers::lost_pass_step_1')}}">¿No te acuerdas?</a>
        @endif

    </header>

    <div id="content" class="wrapper">@yield('content')</div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    @yield('scripts')
</body>
</html>